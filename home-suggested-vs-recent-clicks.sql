/*

Queries to dig into "Recent" vs "Suggested" clicks.

Two queries:
1. distinct userse
2. raw number of events

all grouped by week

Author: Josh Devenny (jdevenny)
*/

-- by distinct user
select
  date_trunc('week', cast(day as date)) as week
  ,count(distinct case when event = 'atlassian.home.search.recent.item.click' then instance||username end) as search_recent_item_click
  ,count(distinct case when event = 'atlassian.home.frequent.item.click' then instance||username end) as yw_suggested_item_click
  ,count(distinct case when event = 'atlassian.home.frequent.show.more' then instance||username end) as yw_suggested_show_more
  ,count(distinct case when event = 'atlassian.home.recent.item.click' then instance||username end) as yw_recent_item_click
  ,count(distinct case when event = 'atlassian.home.container-nav.recent-container.click' then instance||username end) as nav_recent_click
  ,count(distinct case when event = 'atlassian.home.container-nav.linked-app.click' then instance||username end) as nav_app_link_click
  ,count(distinct case when event = 'atlassian.home.notifications.item.click' then instance||username end) as notification_item_clicked
from raw_product.cloud_event_summary
where
  product = 'home'
  and instance not like '%fabric%'
  and instance not like '%.jira-dev.com'
  and instance != 'localhost'
  and date_trunc('week', cast(day as date)) >= current_date - interval '90' day
  and date_trunc('week', cast(day as date)) < date_trunc('week', current_date)
group by 1
order by 1;
    
    
-- by raw # events
select
  date_trunc('week', cast(day as date)) as week
  ,sum(case when event = 'atlassian.home.search.recent.item.click' then count else 0 end) as search_recent_item_click
  ,sum(case when event = 'atlassian.home.frequent.item.click' then count else 0 end) as yw_suggested_item_click
  ,sum(case when event = 'atlassian.home.frequent.show.more' then count else 0 end) as yw_suggested_show_more
  ,sum(case when event = 'atlassian.home.recent.item.click' then count else 0 end) as yw_recent_item_click
  ,sum(case when event = 'atlassian.home.container-nav.recent-container.click' then count else 0 end) as nav_recent_click
  ,sum(case when event = 'atlassian.home.container-nav.linked-app.click' then count else 0 end) as nav_app_link_click
  ,sum(case when event = 'atlassian.home.notifications.item.click' then count else 0 end) as notification_item_clicked
from raw_product.cloud_event_summary
where
  product = 'home'
  and instance not like '%fabric%'
  and instance not like '%.jira-dev.com'
  and instance != 'localhost'
  and date_trunc('week', cast(day as date)) >= current_date - interval '90' day
  and date_trunc('week', cast(day as date)) < date_trunc('week', current_date)
group by 1
order by 1;