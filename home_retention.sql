-- Retention
-- https://redash.data.internal.atlassian.com/queries/19019/source
-- TUTORIAL SEE https://redash.io/help/query-examples-hacks/cohort-example.html 
with weeks as (
  select distinct date_trunc('week',cast(date_week as date)) as week 
  from model.dim_date
  where date_week >= current_date - interval '63' day  --max 9 weeks 
        and date_week < current_date
)
, population AS (
    select date_trunc('week', cast(min(utc_date) as date)) AS cohort_date
        , sen_username  as unique_id
        , max(coalesce(seen_count,0)) as days_returned
    from raw_product.active_users au
    join weeks
        on cast(au.utc_date as date) >= weeks.week
    where 
        product = 'home'
        and metric = 'wau'
        and component = 'new'
        and sen_username is not null 
     group by 2
--     limit 20
   )
, population_agg AS (
     SELECT cohort_date
     , COUNT(distinct unique_id) AS total
     FROM population
     GROUP BY 1
   )
,  returning AS (
   SELECT date_trunc('week', cast(utc_date as date)) AS activity_date
     , case when au.component = 'returning' and au.sen_username = p.unique_id then au.sen_username end AS unique_id
     , min(p.cohort_date) as cohort_date
        , max(coalesce(seen_count,0)) as days_returned
    from population p 
    left outer join raw_product.active_users au on p.unique_id = au.sen_username
    left outer join weeks
        on cast(au.utc_date as date) >= weeks.week
     where 
        au.product = 'home'
        and au.metric = 'wau'
        and au.sen_username is not null 
        and cast(au.utc_date as date) >= p.cohort_date
    group by 1,2
)
 -- final aggregation, exact naming required for cohorts chart?
 SELECT cast(p.cohort_date as varchar(8)) as "date"
       , date_diff('week', p.cohort_date, weeks.week) + 1 as "day_number"   
       , round(1.0*count(distinct case when weeks.week = p.cohort_date then p.unique_id
            else r.unique_id end),0) AS "value"   --start with 100% new users
       , max(ptot.total) as total
       , round(100.0*count(distinct case when weeks.week = p.cohort_date then p.unique_id
            else r.unique_id end)/max(ptot.total),0) AS percentReturning   --start with 100% new users
       , coalesce(approx_percentile(case when weeks.week = p.cohort_date 
            then p.days_returned else r.days_returned end, 0.5),0) as daysReturned 
   FROM population p 
   full outer join weeks on p.cohort_date <= weeks.week
   left outer join returning r 
        on r.unique_id = p.unique_id
        and r.activity_date = weeks.week
   join population_agg ptot on ptot.cohort_date = p.cohort_date
   where p.cohort_date < date_trunc('week', current_date)
   GROUP BY 1,2
   order by 1,2
 ;








