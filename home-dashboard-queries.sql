-- HOME DASHBOARD QUERIES
-- https://redash.data.internal.atlassian.com/dashboard/fabric---home

--1.  MAU/WAU/DAU
select
  utc_date,
  max(case when metric = 'dau' then active_users end) as dau,
  max(case when metric = 'wau' then active_users end) as wau,
  max(case when metric = 'mau' then active_users end) as mau
from raw_product.active_components
where
  product = 'home'
  and cast(utc_date as date) > current_date - interval '60' day
  and cast(utc_date as date) < current_date
group by 1
order by 1;

--2.  WAU Growth Components
select
  utc_date,
  max(case when metric = 'wau' then new_users end) as new,
  max(case when metric = 'wau' then returning_users end) as returning,
  max(case when metric = 'wau' then resurrected_users end) as resurrected,
  -1 * max(case when metric = 'wau' then dormant_users end) as dormant
from raw_product.active_components
where
  product = 'home'
  and cast(utc_date as date) > current_date - interval '2' month
  and day_of_week(cast(utc_date as date)) = 1
group by 1
order by 1;

--3.  DAU/ (WAU or MAU)
select
  utc_date,
  1.0 * max(case when metric = 'dau' then active_users end) / nullif(max(case when metric = 'wau' then active_users end), 0) as dau_over_wau,
  1.0 * max(case when metric = 'dau' then active_users end) / nullif(max(case when metric = 'mau' then active_users end), 0) as dau_over_mau
from raw_product.active_components
where
  product = 'home'
  and cast(utc_date as date) > current_date - interval '60' day
group by 1
order by 1;

--4. Ratio Incoming "New" to Outgoing Dormant
select
  utc_date,
  1.0 * (max(case when metric = 'wau' then new_users end) + max(case when metric = 'wau' then resurrected_users end)) / nullif(max(case when metric = 'wau' then dormant_users end), 0) as wau_quick_ratio,
  1.0 * (max(case when metric = 'mau' then new_users end) + max(case when metric = 'mau' then resurrected_users end)) / nullif(max(case when metric = 'mau' then dormant_users end), 0) as mau_quick_ratio
from raw_product.active_components
where
  product = 'home'
  and cast(utc_date as date) > current_date - interval '30' day
group by 1
order by 1;


--5. Paths out of Home
select
  date_trunc('week', cast(day as date)), substr(event, 16) as event, count(*)
from raw_product.cloud_event
where
  product = 'home'
  and instance not like '%fabric%'
  and instance not like '%.jira-dev.com'
  and instance != 'localhost'
  and date_trunc('week', cast(day as date)) >= current_date - interval '60' day
  and date_trunc('week', cast(day as date)) < date_trunc('week', current_date)
  and event in ('atlassian.home.usermenu.item.profile.click',
    'atlassian.home.usermenu.item.logout.click',
    'atlassian.home.appswitcher.app.link.click',
    'atlassian.home.appswitcher.configure.link.click',
    'atlassian.home.appswitcher.recent.container.click',
    'atlassian.home.createmenu.item.click',
    'atlassian.home.search.recent.item.click',
    'atlassian.home.search.confluence.click',
    'atlassian.home.search.jira.click',
    'atlassian.home.frequent.item.click',
    'atlassian.home.frequent.show.more',
    'atlassian.home.notifications.item.click',
    'atlassian.home.recent.item.click',
    'atlassian.home.container-nav.recent-container.click',
    'atlassian.home.container-nav.linked-app.click',
    'atlassian.home.newsfeed.item.click')
group by 1, 2 order by 1;

--6. Frequent Item Position Distribution
with positions as (
  select
    cast(attributes['position'] as integer) as position
  from raw_product.cloud_event
  where
    event = 'atlassian.home.frequent.item.click'
    and product = 'home'
    and month >= cast(year(current_date) as varchar) || '-' || lpad(cast(month(current_date - interval '2' month) as varchar), 2, '0')
    and instance not in ('product-fabric-testing.atlassian.net', 'product-fabric-testing.jira-dev.com', 'localhost', 'product-fabric.atlassian.net')
)
select
  position, count(*)
from positions
where position is not null
group by 1 order by 1 asc;

--7. Recent Item Position Distribution
with positions as (
  select
    cast(attributes['position'] as integer) as position
  from raw_product.cloud_event
  where
    event = 'atlassian.home.recent.item.click'
    and product = 'home'
    and month >= cast(year(current_date) as varchar) || '-' || lpad(cast(month(current_date - interval '2' month) as varchar), 2, '0')
    and instance not in ('product-fabric-testing.atlassian.net', 'product-fabric-testing.jira-dev.com', 'localhost', 'product-fabric.atlassian.net')
)
select
  position, count(*)
from positions
where position is not null
group by 1 order by 1 asc;

--8. Home Penetration into Conf or JIRA MAU
with user_product_usage as (
  select
    month,
    sen_username,
    sum(case when product = 'home' then 1 else 0 end) as home_usage,
    sum(case when product = 'confluence' then 1 else 0 end) as confluence_usage,
    sum(case when product = 'jira_all' then 1 else 0 end) as jira_usage
  from raw_product.active_users
  where
    cast(utc_date as date) > current_date - interval '180' day
    -- get ready for your eyes to bleed
    and month < cast(year(current_date) as varchar) || '-' || lpad(cast(month(current_date) as varchar), 2, '0')
  group by 1, 2
)
select
  month,
  round(100.0 * sum(case when home_usage > 0 and confluence_usage > 0 then 1 else 0 end) / sum(case when confluence_usage > 0 then 1 else 0 end), 4) as confluence,
  round(100.0 * sum(case when home_usage > 0 and jira_usage > 0 then 1 else 0 end) / sum(case when jira_usage > 0 then 1 else 0 end), 4) as jira,
  25.0 as goal
from user_product_usage
group by 1 order by 1;

--9. Raw Counts Number Dashboard Users Home, Conf, Jira
select
  day,
  count(distinct case when event = 'atlassian.home.recent.view' then instance || username end) as home_dashboard_users,
  count(distinct case when event = 'confluence.spa.rendered' and (attributes['currentSection'] like 'my%' or attributes['currentSection'] like 'discover/%') then instance || username end) as confluence_dashboard_users,
  count(distinct case when event = 'browser.metrics.navigation' and attributes['key'] = 'jira.dashboard' then instance || username end) as jira_dashboard_users
from raw_product.cloud_event
where
  event in ('atlassian.home.recent.view', 'confluence.spa.rendered', 'browser.metrics.navigation')
  and instance not in ('product-fabric-testing.atlassian.net', 'product-fabric-testing.jira-dev.com', 'localhost', 'product-fabric.atlassian.net')
  and cast(day as date) >= current_date - interval '30' day
group by 1 order by 1;

--10.  App Switcher Usage
select
  date_trunc('week', cast(day as date)) as week,
  product || '-' || case when event = 'appswitcher.app.link.click' then 'app'
    when event = 'appswitcher.configure.link.click' then 'configure'
    when event = 'appswitcher.recent.container.click' then 'recent-container' end,
  count(*) as count
from raw_product.cloud_event
where
  product in ('jira', 'confluence')
  and instance not like '%fabric%'
  and instance not like '%.jira-dev.com'
  and instance != 'localhost'
  and date_trunc('week', cast(day as date)) >= current_date - interval '60' day
  and event in (
    'appswitcher.app.link.click',
    'appswitcher.configure.link.click',
    'appswitcher.recent.container.click'
)
group by 1, 2
order by 1;

--11. Notifications Usage
select
  date_trunc('week', cast(day as date)) as week,
  substr(event, 16) as event,
  count(*) as total
from raw_product.cloud_event
where
  product = 'home'
  and instance not like '%fabric%'
  and instance not like '%.jira-dev.com'
  and instance != 'localhost'
  and date_trunc('week', cast(day as date)) >= current_date - interval '180' day
  and date_trunc('week', cast(day as date)) < date_trunc('week', current_date)
  and event in (
    'atlassian.home.notifications.item.click',
    'atlassian.home.notifications.view'
  )
group by 1, 2 order by 1;

-- 12.  People Usage
select
  date_trunc('week', cast(day as date)), substr(event, 16) as event, count(*)
from raw_product.cloud_event
where
  product = 'home'
  and instance not like '%fabric%'--
  and instance not like '%.jira-dev.com'
  and instance != 'localhost'
  and date_trunc('week', cast(day as date)) >= current_date - interval '60' day
  and date_trunc('week', cast(day as date)) < date_trunc('week', current_date)
  and event in (
    'atlassian.home.people-search.click',
    'atlassian.home.profile-page.view',
    'atlassian.home.profile-page.save'
  )
group by 1, 2 order by 1;

--13. News Feed Usage
select
  date_trunc('week', cast(day as date)), substr(event, 16) as event, count(*)
from raw_product.cloud_event
where
  product = 'home'
  and instance not like '%fabric%'
  and instance not like '%.jira-dev.com'
  and instance != 'localhost'
  and date_trunc('week', cast(day as date)) >= current_date - interval '180' day
  and date_trunc('week', cast(day as date)) < date_trunc('week', current_date)
  and event in ('atlassian.home.newsfeed.view',
    'atlassian.home.newsfeed.item.click')
group by 1, 2 order by 1;

--14. Raw Counts Users doing Key Actions Weekly
select
  date_trunc('week', cast(day as date)) as week
  ,count(distinct case when event = 'atlassian.home.notifications.item.click' then instance||username end) as notification_item_clicked
  ,count(distinct case when event = 'atlassian.home.notifications.view' then instance||username end) as notifications_viewed
  ,count(distinct case when event = 'atlassian.home.people-search.view' then instance||username end) as people_search_viewed
  ,count(distinct case when event = 'atlassian.home.people-search.click' then instance||username end) as people_search_click
  ,count(distinct case when event = 'atlassian.home.profile-page.view' then instance||username end) as profile_page_view
  ,count(distinct case when event = 'atlassian.home.newsfeed.view' then instance||username end) as newsfeed_viewed
  ,count(distinct case when event = 'atlassian.home.newsfeed.item.click' then instance||username end) as newsfeed_item_click
  ,count(distinct case when event = 'atlassian.home.recent.view' then instance||username end) as recent_viewed
  ,count(distinct case when event = 'atlassian.home.recent.item.click' then instance||username end) as recent_item_click
from raw_product.cloud_event_summary
where
  product = 'home'
  and instance not like '%fabric%'
  and instance not like '%.jira-dev.com'
  and instance != 'localhost'
  and date_trunc('week', cast(day as date)) >= current_date - interval '90' day
  and date_trunc('week', cast(day as date)) < date_trunc('week', current_date)
group by 1
order by 1, 2 desc;




